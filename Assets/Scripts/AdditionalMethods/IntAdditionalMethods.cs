﻿using System;
using System.Collections;
using System.Collections.Generic;

public static class IntAdditionalMethods
{
    /// <summary>
    /// Turns an int time stamp value into a local time DateTime.
    /// </summary>
    /// <param name="timeStamp"></param>
    /// <returns></returns>
    public static DateTime ToDateTime(this int timeStamp) => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(timeStamp).ToLocalTime();

    public static string ToHex(this int value, int digits) => value.ToString($"X{digits}");


}