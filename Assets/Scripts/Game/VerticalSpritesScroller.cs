using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalSpritesScroller : MonoBehaviour
{
    [field: SerializeField] public float Speed { get; set; } = 7f;

    private float initialSpeed;

    public List<ScrollingSprite> ScrollingSprites { get; } = new List<ScrollingSprite>();

    protected virtual void Start()
    {
        initialSpeed = Speed;
    }

    private void FixedUpdate()
    {
        foreach (var scrollingSpr in ScrollingSprites)
        {
            HandleBeforeScroll(scrollingSpr);
            scrollingSpr.Scroll(Speed, Time.fixedDeltaTime);
            HandleScrolledSpr(scrollingSpr);
        }
    }

    protected virtual void HandleBeforeScroll(ScrollingSprite scrollingSprite) { }

    protected virtual void HandleScrolledSpr(ScrollingSprite scrollingSprite) { }

    public void ResetSpeed() => Speed = initialSpeed;

}
