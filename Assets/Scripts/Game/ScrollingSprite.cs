using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingSprite : MonoBehaviour
{
    [SerializeField] private SpriteRenderer refSprRenderer = null;
    [SerializeField] private float additionalSpeed = 0f;
    public Vector2 Units
    {
        get
        {
            Vector2 dimensions = new Vector2(refSprRenderer.sprite.texture.width, refSprRenderer.sprite.texture.height);
            float pixelsPerUnit = refSprRenderer.sprite.pixelsPerUnit;
            return new Vector2(dimensions.x / pixelsPerUnit, dimensions.y / pixelsPerUnit);
        }
    }

    public void Scroll(float speed, float deltaTime)
    {
        transform.position -= new Vector3(0f, (speed + additionalSpeed) * deltaTime, 0f);
    }

    public bool Equals(ScrollingSprite other)
    {
        return refSprRenderer.sprite.Equals(other.refSprRenderer.sprite);
    }

}
