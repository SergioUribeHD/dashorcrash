using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] private float speed = 7f;

    private float horizontal;
    private float vertical;
    private Vector2 movement;

    public bool Enabled { get; set; } = true;

    private void FixedUpdate()
    {
        Move();
    }

    private void Update()
    {
        HandleInputs();
    }


    private void HandleInputs()
    {
        horizontal = Enabled ? Input.GetAxisRaw("Horizontal") : 0;
        vertical = Enabled ? Input.GetAxisRaw("Vertical") : 0;
        movement.x = horizontal;
        movement.y = vertical;
    }

    private void Move()
    {
        transform.position += new Vector3(movement.x, movement.y, 0f) * speed * Time.fixedDeltaTime;
    }
}
