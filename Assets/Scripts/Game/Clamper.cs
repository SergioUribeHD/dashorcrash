using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clamper : RectRange
{
    [SerializeField] private Transform clampedObject = null;

    private void Update()
    {
        Vector3 newPos;
        newPos.x = Mathf.Clamp(clampedObject.transform.position.x, MinAxes.x, MaxAxes.x);
        newPos.y = Mathf.Clamp(clampedObject.transform.position.y, MinAxes.y, MaxAxes.y);
        newPos.z = clampedObject.transform.position.z;
        clampedObject.transform.position = newPos;
    }

}
