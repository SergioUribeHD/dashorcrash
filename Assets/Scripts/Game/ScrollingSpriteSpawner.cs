using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingSpriteSpawner : MonoBehaviour
{
    [SerializeField] private RectRange[] spawnZones = null;
    [SerializeField] private RecursiveVerticalSprsScroller verticalScroller = null;
    [SerializeField] private ScrollingSprite[] scrollingSprPrefabs = new ScrollingSprite[0];
    [SerializeField] private float minSpawnRate = 0.7f;
    [SerializeField] private float maxSpawnRate = 1.5f;


    private List<ScrollingSprite> usedPrefabs = new List<ScrollingSprite>();

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSpawnRate, maxSpawnRate));
        foreach (var spawnZone in spawnZones)
            SpawnRandomScrollingSpr(spawnZone);
        }
    }

    private void SpawnRandomScrollingSpr(RectRange spawnZone)
    {
        if (usedPrefabs.Count == 0) usedPrefabs.AddRange(scrollingSprPrefabs);
        var randomSpr = usedPrefabs.GetRandomItem(true);
        bool unused = verticalScroller.ScrollingSprites.TryFind(s => s.Equals(randomSpr) && !s.gameObject.activeSelf, out var match);
        if (unused)
        {
            match.gameObject.SetActive(true);
            match.transform.position = spawnZone.GetRandomPosWithinRange(match.Units.x);
        }
        else
        {
            Vector3 pos = spawnZone.GetRandomPosWithinRange(randomSpr.Units.x);
            Transform parent = verticalScroller.ActiveLimitArea.transform;
            var scrollingSpr = Instantiate(randomSpr, pos, randomSpr.transform.rotation, parent);
            verticalScroller.ScrollingSprites.Add(scrollingSpr);
        }
    }

}
